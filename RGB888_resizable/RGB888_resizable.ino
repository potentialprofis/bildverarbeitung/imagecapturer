/*
Author: Niklas Weber
Institution: Nuremberg Institute of Technology
Last Change Date: 16. June 2024 (first implementation)
Based on projects:
  https://randomnerdtutorials.com/esp32-timer-wake-up-deep-sleep/#:~:text=The%20ESP32%20can%20go%20into,while%20maintaining%20low%20power%20consumption.
  https://randomnerdtutorials.com/esp32-cam-ov2640-camera-settings/
  https://github.com/alanesq/esp32cam-demo/tree/master
  https://dronebotworkshop.com/esp32-cam-microsd/
  
Project Name: eChickenwire
*/

// Camera libraries
#include "esp_camera.h"
#include "soc/soc.h"
#include "soc/rtc_cntl_reg.h"
#include "driver/rtc_io.h"

// MicroSD Libraries
#include "FS.h"
#include "SD_MMC.h"

// EEPROM Library
#include "EEPROM.h"

// Use 2 byte of EEPROM space
#define EEPROM_SIZE 2

// Counter for picture number
uint16_t pictureCount = 0;

uint8_t  pictureCountMSByte = 0;
uint8_t  pictureCountLSByte = 0; 


// Pin definitions for CAMERA_MODEL_AI_THINKER
#define PWDN_GPIO_NUM     32
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27

#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22

#define uS_TO_S_FACTOR 1000000  /* Conversion factor for micro seconds to seconds */
#define S_TO_MIN_FACTOR 1
#define TIME_TO_SLEEP  2      /* Time ESP32 will go to sleep (in minutes) */

RTC_DATA_ATTR int bootCount = 0;

uint8_t* rgbArray;
const int width = 160; // Example: QQVGA resolution
const int height = 120;

// Function to convert RGB565 to RGB888
void convertRGB565ToRGB888(const uint8_t* buf, size_t len, uint8_t* rgbArray) {
  for (size_t i = 0; i < len; i += 2) {
    uint16_t pixel = (buf[i] << 8) | buf[i + 1];

    uint8_t r = (pixel & 0b1111100000000000) >> 11;
    uint8_t g = (pixel & 0b0000011111100000) >> 5;
    uint8_t b = (pixel & 0b0000000000011111);

    r = (r << 3) | (r >> 2); // Expand 5-bit red to 8-bit
    g = (g << 2) | (g >> 4); // Expand 6-bit green to 8-bit
    b = (b << 3) | (b >> 2); // Expand 5-bit blue to 8-bit

    size_t j = i / 2;
    rgbArray[j * 3] = r;
    rgbArray[j * 3 + 1] = g;
    rgbArray[j * 3 + 2] = b;
  }
}

// Function to configure the ESP32 camera
void configESPCamera() {
  camera_config_t config;

  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_RGB565; // Choices are YUV422, GRAYSCALE, RGB565, JPEG

  // Select lower frame size if the camera doesn't support PSRAM
  if (psramFound()) {
    config.frame_size = FRAMESIZE_QQVGA; // Reduced resolution
    config.jpeg_quality = 10; // 10-63 lower number means higher quality
    config.fb_count = 2;
  } else {
    config.frame_size = FRAMESIZE_QQVGA;
    config.jpeg_quality = 12;
    config.fb_count = 1;
  }

  // Initialize the Camera
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", err);
    return;
  }

  // Camera quality adjustments
  sensor_t * s = esp_camera_sensor_get();

  // Adjust camera settings here if needed

  // Disable flash LED by setting the GPIO pin low
  pinMode(33, OUTPUT);  // GPIO 4 is the flash pin
  digitalWrite(33, LOW);
}

/*
 * Resize an image using bilinear interpolation
 * src: Pointer to the source image array
 * srcWidth, srcHeight: Dimensions of the source image
 * dest: Pointer to the destination image array
 * destWidth, destHeight: Dimensions of the destination image
 */
void resizeImage(uint8_t* src, int srcWidth, int srcHeight, uint8_t* dest, int destWidth, int destHeight) {
  for (int y = 0; y < destHeight; y++) {
    for (int x = 0; x < destWidth; x++) {
      // Compute the corresponding coordinates in the source image
      float gx = x * (srcWidth - 1) / (float)(destWidth - 1);
      float gy = y * (srcHeight - 1) / (float)(destHeight - 1);

      // Get the top-left corner
      int gxi = (int)gx;
      int gyi = (int)gy;

      // Get the fractional parts
      float dx = gx - gxi;
      float dy = gy - gyi;

      // Get the four neighboring pixels
      uint8_t* p00 = &src[(gyi * srcWidth + gxi) * 3];
      uint8_t* p10 = &src[(gyi * srcWidth + gxi + 1) * 3];
      uint8_t* p01 = &src[((gyi + 1) * srcWidth + gxi) * 3];
      uint8_t* p11 = &src[((gyi + 1) * srcWidth + gxi + 1) * 3];

      for (int c = 0; c < 3; c++) {
        // Perform the bilinear interpolation
        float color = p00[c] * (1 - dx) * (1 - dy) +
                      p10[c] * dx * (1 - dy) +
                      p01[c] * (1 - dx) * dy +
                      p11[c] * dx * dy;

        dest[(y * destWidth + x) * 3 + c] = (uint8_t)color;
      }
    }
  }
}


// Function to initialize the microSD card
void initMicroSDCard() {
  Serial.println("Mounting MicroSD Card");
  if (!SD_MMC.begin()) {
    Serial.println("MicroSD Card Mount Failed");
    return;
  }
  uint8_t cardType = SD_MMC.cardType();
  if (cardType == CARD_NONE) {
    Serial.println("No MicroSD Card found");
    return;
  }
}

// Function to save the RGB data as a BMP file
void saveRGBToFile(const char* filename, uint8_t* rgbArray, int width, int height) {
  fs::FS &fs = SD_MMC;
  File file = fs.open(filename, FILE_WRITE);
  if (!file) {
    Serial.println("Failed to open file");
    return;
  }

  // Write BMP header
  uint32_t fileSize = 54 + width * height * 3; // Header + RGB data
  uint32_t dataOffset = 54; // Data offset

  // BMP header
  uint8_t bmpHeader[54] = {
    'B', 'M', // Signature
    (uint8_t)(fileSize), (uint8_t)(fileSize >> 8), (uint8_t)(fileSize >> 16), (uint8_t)(fileSize >> 24), // File size
    0, 0, 0, 0, // Reserved
    (uint8_t)(dataOffset), 0, 0, 0, // Data offset
    40, 0, 0, 0, // Header size
    (uint8_t)(width), (uint8_t)(width >> 8), (uint8_t)(width >> 16), (uint8_t)(width >> 24), // Image width
    (uint8_t)(height), (uint8_t)(height >> 8), (uint8_t)(height >> 16), (uint8_t)(height >> 24), // Image height
    1, 0, // Color planes
    24, 0, // Bits per pixel
    0, 0, 0, 0, // Compression
    0, 0, 0, 0, // Image size (uncompressed)
    0, 0, 0, 0, // Horizontal resolution
    0, 0, 0, 0, // Vertical resolution
    0, 0, 0, 0, // Colors in color table
    0, 0, 0, 0  // Important color count
  };

  file.write(bmpHeader, 54);

  // Write RGB data (BGR format for BMP)
  for (int y = height - 1; y >= 0; y--) { // BMP stores rows from bottom to top
    for (int x = 0; x < width; x++) {
      int index = (y * width + x) * 3;
      file.write(rgbArray[index + 2]); // Blue
      file.write(rgbArray[index + 1]); // Green
      file.write(rgbArray[index]);     // Red
    }
  }

  file.close();
  Serial.println("Image successfully saved");
}


void captureAndResizePhoto(String path, int newWidth, int newHeight) {
  camera_fb_t* fb = esp_camera_fb_get();
  if (!fb) {
    Serial.println("Failed to capture photo");
    return;
  }

  // Allocate memory for the original and resized RGB arrays
  uint8_t* originalRGBArray = new uint8_t[width * height * 3];
  uint8_t* resizedRGBArray = new uint8_t[newWidth * newHeight * 3];

  // Convert RGB565 to RGB888
  convertRGB565ToRGB888(fb->buf, fb->len, originalRGBArray);

  // Resize the image
  resizeImage(originalRGBArray, width, height, resizedRGBArray, newWidth, newHeight);

  // Save the resized image as BMP
  saveRGBToFile(path.c_str(), resizedRGBArray, newWidth, newHeight);

  // Clean up
  delete[] originalRGBArray;
  delete[] resizedRGBArray;
  esp_camera_fb_return(fb);

  Serial.println("Photo captured, resized and saved");
}

// Function to print the reason by which ESP32 has been awaken from sleep
void print_wakeup_reason() {
  esp_sleep_wakeup_cause_t wakeup_reason;
  wakeup_reason = esp_sleep_get_wakeup_cause();
  switch (wakeup_reason) {
    case ESP_SLEEP_WAKEUP_EXT0: Serial.println("Wakeup caused by external signal using RTC_IO"); break;
    case ESP_SLEEP_WAKEUP_EXT1: Serial.println("Wakeup caused by external signal using RTC_CNTL"); break;
    case ESP_SLEEP_WAKEUP_TIMER: Serial.println("Wakeup caused by timer"); break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD: Serial.println("Wakeup caused by touchpad"); break;
    case ESP_SLEEP_WAKEUP_ULP: Serial.println("Wakeup caused by ULP program"); break;
    default: Serial.printf("Wakeup was not caused by deep sleep: %d\n", wakeup_reason); break;
  }
}

void setup() {
  // Init Serial Monitor
  Serial.begin(115200);
  delay(1000); // Give time to open the serial monitor

  // Increment boot number and print it every reboot
  ++bootCount;
  //Serial.println("Boot number: " + String(bootCount));

  // Print the wakeup reason for ESP32
  //print_wakeup_reason();

  // Initializing EEPROM
  EEPROM.begin(EEPROM_SIZE);

  // Load picture number from EEPROM
  pictureCount = EEPROM.read(0) | (EEPROM.read(1) << 8) ;

  // Initialize the camera
  configESPCamera();

  // Initialize the microSD card
  //initMicroSDCard();

  // Take and save a new photo
  String path = "/picture" + String(pictureCount) + ".bmp";
  //captureAndResizePhoto(path, 320, 240); // Example: resize to 320x240

  Serial.println("String path: " + path);

  // Increment picture number and store it in EEPROM
  pictureCount++;
  pictureCountMSByte = (pictureCount)>>8;
  pictureCountLSByte = (pictureCount);
  EEPROM.write(0,pictureCountLSByte);
  EEPROM.write(1,pictureCountMSByte);
  EEPROM.commit();

  // Set ESP32 to wake up every 5 minutes
  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP /* S_TO_MIN_FACTOR*/ * uS_TO_S_FACTOR);
  //Serial.println("Setup ESP32 to sleep for every " + String(TIME_TO_SLEEP) + " minutes");

  //Serial.println("Going to sleep now");
  delay(900);
  Serial.flush();
  esp_deep_sleep_start();
}

void loop() {
  // This will never be reached
}