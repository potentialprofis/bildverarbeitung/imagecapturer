
# ImageCapturer
***Niklas Weber***

*created on 18.06.2024*

***Project eChickenwire*** 

Based on projects:
  - https://randomnerdtutorials.com/esp32-timer-wake-up-deep-sleep#:~:text=The%20ESP32%20can%20go%20into,while%20maintaining%20low%20power%20consumption
  - https://randomnerdtutorials.com/esp32-cam-ov2640-camera-settings/
  - https://github.com/alanesq/esp32cam-demo/tree/master
 -  https://dronebotworkshop.com/esp32-cam-microsd/
  

### Detailed Explanation of the Program

This program is designed to capture images using an ESP32 camera, resize the images, save them to a microSD card, and put the ESP32 into deep sleep mode to save power. The device wakes up periodically to repeat the process. Below is a detailed explanation of each part of the code.

#### Include Libraries


```cpp
// Camera libraries
#include "esp_camera.h"
#include "soc/soc.h"
#include "soc/rtc_cntl_reg.h"
#include "driver/rtc_io.h"

// MicroSD Libraries
#include "FS.h"
#include "SD_MMC.h"

// EEPROM Library
#include "EEPROM.h"
```

**Included Libraries:**
- **esp_camera.h:** Provides functions for camera operations.
- **soc/soc.h, soc/rtc_cntl_reg.h, driver/rtc_io.h:** For low-level system control and RTC operations.
- **FS.h, SD_MMC.h:** For file system and microSD card operations.
- **EEPROM.h:** For reading and writing data to the EEPROM.

#### Definitions and Global Variables


```cpp
// Use 1 byte of EEPROM space
#define EEPROM_SIZE 1

// Counter for picture number
unsigned int pictureCount = 0;

// Pin definitions for CAMERA_MODEL_AI_THINKER
#define PWDN_GPIO_NUM     32
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27

#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22

#define uS_TO_S_FACTOR 1000000  /* Conversion factor for micro seconds to seconds */
#define S_TO_MIN_FACTOR 60
#define TIME_TO_SLEEP  5       /* Time ESP32 will go to sleep (in minutes) */

RTC_DATA_ATTR int bootCount = 0;

uint8_t* rgbArray;
const int width = 160; // Example: QQVGA resolution
const int height = 120;
```


**Definitions and Global Variables:**
- `EEPROM_SIZE`: Defines the EEPROM size to use.
- `pictureCount`: Counter to keep track of the number of pictures taken. Is used in the name, the picture will be saved as on the SD Card
- **Pin Definitions:** Define the GPIO pins for the ESP32 camera module.
- **Constants for Sleep Duration:** Define the conversion factors and sleep time for the ESP32.
- `RTC_DATA_ATTR int bootCount`: Keeps track of the number of reboots (For debugging purposes).
- `rgbArray`, `width`, `height`: Used for image processing.

#### Convert RGB565 to RGB888


```cpp
void convertRGB565ToRGB888(const uint8_t* buf, size_t len, uint8_t* rgbArray) {
  for (size_t i = 0; i < len; i += 2) {
    uint16_t pixel = (buf[i] << 8) | buf[i + 1];

    uint8_t r = (pixel & 0b1111100000000000) >> 11;
    uint8_t g = (pixel & 0b0000011111100000) >> 5;
    uint8_t b = (pixel & 0b0000000000011111);

    r = (r << 3) | (r >> 2); // Expand 5-bit red to 8-bit
    g = (g << 2) | (g >> 4); // Expand 6-bit green to 8-bit
    b = (b << 3) | (b >> 2); // Expand 5-bit blue to 8-bit

    size_t j = i / 2;
    rgbArray[j * 3] = r;
    rgbArray[j * 3 + 1] = g;
    rgbArray[j * 3 + 2] = b;
  }
}
```
**Function Explanation:**
- **convertRGB565ToRGB888:** Converts an image from RGB565 format (used by the camera) to RGB888 format.

![alt text](images/RGB888vsRGB888.png)

As can be seen in this image from [Forlinx](https://www.forlinx.net/industrial-news/difference-between-rgb565-and-rgb888-423.html) the difference between the RGB565 Format and the RGB888-Format is, the number of Bits, the rgb-values are stored in. In the RGB888-Format, R-,G- and B-values each have 8 bits, to store the data. In the RGB565-Format there are (as the name might hint at) 5 Bits for R, 6 Bits for G and again 5 Bists for the B-value, so that they can be stored inside of a 16-bit variables.

So to transform the values from RGB565 to RGB888, first each of the color-values (R,G and B) need to be isolated. Initialy they are all stored inside a uint16_t value called `pixel`. 

This is done by taking the `i`'st position of the pixel-array, left-shifting it 8 and then logically linking it with an 'or'-Function to the next higher value of the array, effectively combining the two 8-Bit values into one 16-Bit value:

```cpp
    uint16_t pixel = (buf[i] << 8) | buf[i + 1];
```

Then, to isolate the r- g- and b- values, the `pixel`-value is logically linked with an 'and'-function with a Bitmask for each color-value (according to the bit positions int the picture above) and then shifted to the right position:
```cpp
    uint8_t r = (pixel & 0b1111100000000000) >> 11;
    uint8_t g = (pixel & 0b0000011111100000) >> 5;
    uint8_t b = (pixel & 0b0000000000011111);

```

Lastly, the color-values need to be expanded to a 8-bit value. For example `00011111` needs to become `11111111`. This can be done by first shifting the value 3 to the left and the logically linking the value with the or-function with the value shifted 2 to the right e.G.:

1. `00011111`


2. `11111000` (shifted 3 to the left)

or

3. `00000111` (shited 2 to the right)

makes

4. `11111111`

This works with the 5-Bit values. For the 6-Bit values it must be shifted 2 to the right and then 4 to the left.

```cpp
    r = (r << 3) | (r >> 2); // Expand 5-bit red to 8-bit
    g = (g << 2) | (g >> 4); // Expand 6-bit green to 8-bit
    b = (b << 3) | (b >> 2); // Expand 5-bit blue to 8-bit
```

Lastly the values are put back into an uint8_t-array. Where the values are stored back to back in pairs of three:

```cpp
    size_t j = i / 2;
    rgbArray[j * 3] = r;
    rgbArray[j * 3 + 1] = g;
    rgbArray[j * 3 + 2] = b;
```



#### Configure the ESP32 Camera


```cpp
void configESPCamera() {
  camera_config_t config;

  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_RGB565; // Choices are YUV422, GRAYSCALE, RGB565, JPEG

  if (psramFound()) {
    config.frame_size = FRAMESIZE_QQVGA; // Reduced resolution
    config.jpeg_quality = 10; // 10-63 lower number means higher quality
    config.fb_count = 2;
  } else {
    config.frame_size = FRAMESIZE_QQVGA;
    config.jpeg_quality = 12;
    config.fb_count = 1;
  }

  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", err);
    return;
  }

  sensor_t * s = esp_camera_sensor_get();
}
```

**Function Explanation:**
- **configESPCamera:** Configures the ESP32 camera with appropriate settings for resolution, pin connections, and other parameters.

#### Resize Image Using Bilinear Interpolation


```cpp
/*
 * Resize an image using bilinear interpolation
 * src: Pointer to the source image array
 * srcWidth, srcHeight: Dimensions of the source image
 * dest: Pointer to the destination image array
 * destWidth, destHeight: Dimensions of the destination image
 */
void resizeImage(uint8_t* src, int srcWidth, int srcHeight, uint8_t* dest, int destWidth, int destHeight) {
  for (int y = 0; y < destHeight; y++) {
    for (int x = 0; x < destWidth; x++) {
      // Compute the corresponding coordinates in the source image
      float gx = x * (srcWidth - 1) / (float)(destWidth - 1);
      float gy = y * (srcHeight - 1) / (float)(destHeight - 1);

      // Get the top-left corner
      int gxi = (int)gx;
      int gyi = (int)gy;

      // Get the fractional parts
      float dx = gx - gxi;
      float dy = gy - gyi;

      // Get the four neighboring pixels
      uint8_t* p00 = &src[(gyi * srcWidth + gxi) * 3];
      uint8_t* p10 = &src[(gyi * srcWidth + gxi + 1) * 3];
      uint8_t* p01 = &src[((gyi + 1) * srcWidth + gxi) * 3];
      uint8_t* p11 = &src[((gyi + 1) * srcWidth + gxi + 1) * 3];

      for (int c = 0; c < 3; c++) {
        // Perform the bilinear interpolation
        float color = p00[c] * (1 - dx) * (1 - dy) +
                      p10[c] * dx * (1 - dy) +
                      p01[c] * (1 - dx) * dy +
                      p11[c] * dx * dy;

        dest[(y * destWidth + x) * 3 + c] = (uint8_t)color;
      }
    }
  }
}
```

**Detailed Explanation of Bilinear Interpolation:**

Bilinear Interpolation is used to resize images by calculating the color values for each pixel in the resized image based on the corresponding pixels in the original image. This involves a weighted average of the four nearest pixels in the source image, which provides a smooth transition and reduces aliasing artifacts.

In the code, the function `resizeImage` performs bilinear interpolation as follows:
- For each pixel in the destination image, it calculates the corresponding coordinates in the source image.
- It identifies the four nearest pixels in the source image.
- It calculates the color values of the destination pixel by performing a weighted average of these four source pixels.

This approach ensures that the resized image maintains a good level of detail and visual quality, even at a lower resolution.

#### Image Saving and Deep Sleep Functionality

The remaining parts of the code handle image saving to the microSD card and putting the ESP32 into deep sleep mode to conserve power. These functionalities are crucial for applications where the ESP32 operates on battery power and needs to perform periodic tasks while minimizing energy consumption.

### Conclusion

This program effectively demonstrates how to use the ESP32 camera module to capture, process, and store images while implementing power-saving techniques. By following the detailed explanations and code snippets provided, one can gain a thorough understanding of the steps involved in configuring the ESP32 camera, resizing images, and managing power consumption through deep sleep mode.




